import h5py as h5
import os
from enum import Enum
from collections import namedtuple
from multiprocessing import Pool as ProcessPool
from pathlib import Path
from typing import IO
from struct import Struct

import numpy as np

_this_dir = Path(__file__).parent
_data_dir = _this_dir / "data"


DataHeader = namedtuple("DataHeader", [
    "Type",
    "TimestampUpper",
    "TimestampLower",
    "Channel",
    "Unit",
    "NoWf",
    "NoWoWf"
])
data_header_struct = Struct("hHIhhhh")


class DataBlockType(Enum):
    SPIKE = 1
    EVENT = 4
    SLOW = 5


def main(plx_path: Path, hdf_path: Path):
    with open(plx_path, "rb") as plx_file:
        file_header = read_file_header(plx_file)
        ts_counts, wf_counts, ev_counts = np.fromfile(plx_file, counts_dtype, 1)[0]
        spike_headers = np.fromfile(plx_file, spike_header_dtype, file_header["NumDSPChannels"])
        slow_headers = np.fromfile(plx_file, slow_header_dtype, file_header["NumSlowChannels"])
        event_headers = np.fromfile(plx_file, event_header_dtype, file_header["NumEventChannels"])

        data_offset = plx_file.tell()
        # data_length = plx_file.seek(0, 2) - data_offset

        print(f"TSCounts: {np.sum(ts_counts)}")
        print(f"WFCounts: {np.sum(wf_counts)}")
        print(f"EVCounts: {np.sum(ev_counts)}")
        print(f"Read {len(spike_headers)} spike headers")
        print(f"Read {len(slow_headers)} slow headers")
        print(f"Read {len(event_headers)} event headers")

    last_timestamp = file_header["LastTimestamp"]
    batch_size = 2**20

    index_offsets = np.arange(10) * batch_size
    locate_params = (
        (plx_path, data_offset, last_timestamp, io, batch_size)
        for io in index_offsets
    )

    with h5.File(hdf_path, "w") as hdf_file:
        hdf_file.attrs.update(file_header)
        hdf_file.attrs["TSCounts"] = ts_counts
        hdf_file.attrs["WFCounts"] = wf_counts
        hdf_file.attrs["EVCounts"] = ev_counts

    with ProcessPool(os.cpu_count() - 1) as pool:
        data_headers = np.concatenate(pool.starmap(locate_data_headers, locate_params))

    return


def locate_data_headers(plx_path: Path, data_offset: int, last_timestamp: int, index_offset: int, batch_size: int) -> np.ndarray:

    likely_data_headers = mapped_data_headers(data_offset, plx_path)[index_offset:index_offset + batch_size]
    likely_timestamps = (
        likely_data_headers['TimestampUpper'].astype(np.int64) << 32
        + likely_data_headers['TimestampLower']
    )

    relative_indices = np.flatnonzero(
        (
            (likely_data_headers['Type'] == DataBlockType.SPIKE.value) |
            (likely_data_headers['Type'] == DataBlockType.EVENT.value) |
            (likely_data_headers['Type'] == DataBlockType.SLOW.value)
        ) &
        (likely_timestamps < last_timestamp) &
        (likely_data_headers['Channel'] >= 0) &
        (likely_data_headers['Unit'] >= 0) &
        (
            (likely_data_headers['NoWf'] == 0) |
            (likely_data_headers['NoWf'] == 1)
        ) &
        (likely_data_headers['NoWoWf'] >= 0)
    )

    result_dtype = np.dtype([
        ("Index", relative_indices.dtype),
        ("Type", np.uint8),
        ("Timestamp", np.uint64),
        ("Channel", np.uint16),
        ("Unit", np.uint8),
        ("Words", np.uint16),
    ])
    data_headers = np.empty(len(relative_indices), dtype=result_dtype)

    data_headers["Index"] = index_offset + relative_indices
    data_headers["Type"] = likely_data_headers["Type"][relative_indices]
    data_headers["Timestamp"] = likely_timestamps[relative_indices]
    data_headers["Channel"] = likely_data_headers["Channel"][relative_indices]
    data_headers["Unit"] = likely_data_headers["Unit"][relative_indices]
    data_headers["Words"] = likely_data_headers['NoWf'][relative_indices] * likely_data_headers['NoWoWf'][relative_indices]

    return data_headers


def mapped_data_headers(data_offset, plx_path):
    return np.memmap(plx_path, data_header_dtype, "r", offset=data_offset)


def read_file_header(plx_file: IO[bytes]) -> "np.ndarray[file_header_dtype]":
    keys = file_header_dtype.fields.keys()
    values = np.fromfile(plx_file, file_header_dtype, 1)[0]
    return dict(zip(keys, values))


def file_size(path: Path):
    return os.stat(path).st_size


counts_dtype = np.dtype([
    ("TSCounts", "(130, 5) i4"),
    ("WFCounts", "(130, 5) i4"),
    ("EVCounts", "(512,) i4"),
])

data_header_dtype = np.dtype([
    ("Type", "i2"),
    ("TimestampUpper", "u2"),
    ("TimestampLower", "u4"),
    ("Channel", "i2"),
    ("Unit", "i2"),
    ("NoWf", "i2"),
    ("NoWoWf", "i2"),
])

event_header_dtype = np.dtype([
    ("Name", "S32"),
    ("Channel", "i4"),
    ("Comment", "S128"),
    ("Padding", "S132"),
])

file_header_dtype = np.dtype([
    ("MagicNumber", "u4"),
    ("Version", "i4"),
    ("Comment", "S128"),
    ("ADFrequency", "i4"),
    ("NumDSPChannels", "i4"),
    ("NumEventChannels", "i4"),
    ("NumSlowChannels", "i4"),
    ("NumPointsWave", "i4"),
    ("NumPointsPreThr", "i4"),
    ("Year", "i4"),
    ("Month", "i4"),
    ("Day", "i4"),
    ("Hour", "i4"),
    ("Minute", "i4"),
    ("Second", "i4"),
    ("FastRead", "i4"),
    ("WaveformFreq", "i4"),
    ("LastTimestamp", "f8"),
    ("Trodalness", "i1"),
    ("DataTrodalness", "i1"),
    ("BitsPerSpikeSample", "i1"),
    ("BitsPerSlowSample", "i1"),
    ("SpikeMaxMagnitudeMV", "u2"),
    ("SlowMaxMagnitudeMV", "u2"),
    ("SpikePreAmpGain", "u2"),
    ("Padding", "S46"),
])

slow_header_dtype = np.dtype([
    ("Name", "S32"),
    ("Channel", "i4"),
    ("ADFreq", "i4"),
    ("Gain", "i4"),
    ("Enabled", "i4"),
    ("PreAmpGain", "i4"),
    ("SpikeChannel", "i4"),
    ("Comment", "S128"),
    ("Padding", "S112"),
])

spike_header_dtype = np.dtype([
    ("Name", "S32"),
    ("SIGName", "S32"),
    ("Channel", "i4"),
    ("WFRate", "i4"),
    ("SIG", "i4"),
    ("Ref", "i4"),
    ("Gain", "i4"),
    ("Filter", "i4"),
    ("Threshold", "i4"),
    ("Method", "i4"),
    ("NUnits", "i4"),
    ("Template", "(5, 64) i2"),
    ("Fit", "(5,) i4"),
    ("SortWidth", "i4"),
    ("Boxes", "(5, 2, 4) i2"),
    ("SortBeg", "i4"),
    ("Comment", "S128"),
    ("Padding", "S44"),
])


if __name__ == "__main__":
    main(_data_dir / "grating-v206-he1800-hd2600.plx", "tmp.h5")
